# BlattleTanks
## A project made to work with the Robocode program for a test

O meu robô principal **_RickBo_** ele foi um robô pensado para ser móvel e que pudesse fazer um dano consideravel aos robôs inimigos.
apesar de ele ser feito apenas com os códigos mais simples do [RoboCode API](https://robocode.sourceforge.io/docs/robocode/robocode/package-summary.html) ele é bastante util contra certos tipos de robôs e em certas situações e pode ser que fique por volta de top 10 contra alguns dos robôs de exemplo que são disponibilizados no aplicativo. <!-- nos meus testes ele ficou apenas no top 5 ¯\_(ツ)_/¯ -->

> Pontos Fortes
1. O robô se dá bem tanto no meio do canpo de batalha quanto no canto
1. O robô é capaz de rapidamente batalhar em 360º contra os inimigos em volta
1. O movimento dele é simples porém suficiente para desviar de balas vindo na direção dele
1. Em batalhas de perto ele possiu "certa" vantagem
1. Quando ele acerta um inimigo fisicamente há 50% de chance que ele acerte um projétil nivel 2
1. Ele se destaca em batalhas pequenas com menos de 10 inimigos e uma arena com menos de 1000x1000

> Pontos Fracos
1. Se o Robô estiver muito proximo dos cantos pode ser que ele tenha problemas saindo de lá e possa se tornar um alvo facil
2. Quando ele acerta um inimigo fisicamente há 50% de chance que ele erre um projétil nivel 2  
2. Se um inimigo move-se apenas em linha reta ele vai ter problemas em acertar a menos que esteja indo na direção dele ou contrária
2. pode ficar preso nas pontas do mapa 
2. Se tiver muitos inimigos ele vai atirar muito, consequentemente levanto ao esgotamento de energia
2. Não consegui pensar em uma forma eficiente de lidar com o dano recebido