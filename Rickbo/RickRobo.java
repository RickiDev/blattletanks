package Rickbo;
import robocode.*;
import java.awt.Color;
/**
 * RickRobo - a simple robot by Richard S. Rankl
 */
public class RickRobo extends Robot
{

	public void run() {
		setColors(Color.black,Color.gray,Color.green); // body,gun,radar

		// Robot main loop
		while(true) {
			for(int i=0; i<=5;i++){
			 ahead(80);
			 turnRight(80);
			 turnGunRight(36);
			}
			for(int i=0; i<=5;i++){
			 back(80);
			 turnLeft(80);
			 turnGunLeft(36);			
			}
			turnGunRight(180);
		}
	}

	
	public void onScannedRobot(ScannedRobotEvent e) {
	    fire(1);
		
}

	public void onHitByBullet(HitByBulletEvent e) {
		//se eu tentar fazer alguma ação ele para assim que atingido
}

	public void onHitWall(HitWallEvent e) {
		turnLeft(30);
		turnGunRight(90);
	}	
	
    public void onHitRobot(HitRobotEvent e) {
	    fire(2);
	    turnGunLeft(90);
	}
	
public void onBattleEnded(BattleEndedEvent e) {
       out.println("the battles are random, if i didnot win top 10 do it again");
   }
}
